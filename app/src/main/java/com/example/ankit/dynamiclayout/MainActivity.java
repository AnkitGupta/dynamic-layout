package com.example.ankit.dynamiclayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_IMAGE = 10;
    int mBaseViewHeight;
    int mBaseViewWidth;

    int mBaseImageWidth;
    int mBaseImageHeight;

    ImageView mBaseImageView;
    ImageView mSelectedImageView;
    TextView mTextInfo;

    float mRatio;
    float mTranslateX = 0;
    float mTranslateY = 0;

    ConstraintLayout mConstraintLayout;
    List<Element> mElements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextInfo = (TextView) findViewById(R.id.text_info);
        mConstraintLayout = (ConstraintLayout) findViewById(R.id.my_layout);

        mBaseImageView  = (ImageView) findViewById(R.id.image_base);
        mBaseImageView.setImageResource(R.drawable.base_layout_1920_1280);

        mElements = new ArrayList<>();
        Element e1 = new Element("element-1", 246, 146, 570, 508);
        Element e2 = new Element("element-2", 1000, 600, 600, 585);
        mElements.add(e1);
        mElements.add(e2);

        mBaseImageView.post(new Runnable() {
            @Override
            public void run() {
                mBaseViewWidth = mBaseImageView.getWidth();
                mBaseViewHeight = mBaseImageView.getHeight();

                // From API
                mBaseImageWidth = 1920;
                mBaseImageHeight = 1280;
                updateTextInfo("Layout image dimension:" + mBaseImageWidth + "x" + mBaseImageHeight, null);
                updateTextInfo("Layout view dimension:" + mBaseViewWidth + "x" + mBaseViewHeight, null);

                init();
                drawElements();
            }
        });
    }

    private void init() {
        // Base image is set to FitCenter
        if (mBaseImageWidth > mBaseImageHeight) {
            mRatio = Math.min(mBaseViewWidth *1.0f / mBaseImageWidth, 1.0f);
            mTranslateY = mBaseViewHeight - mRatio * mBaseImageHeight;
        } else {
            mRatio = Math.min(mBaseViewHeight*1.0f / mBaseImageHeight, 1.0f);
            mTranslateX = mBaseViewWidth - mRatio * mBaseImageWidth;
        }
    }

    private void drawElements() {
        int i = 0;
        for (Element e : mElements) {
            ImageView ie = generateElement(e);
            ie.setImageResource(R.drawable.default_placeholder);
            ie.setTag(i++);
            ie.setOnClickListener(this);
            mConstraintLayout.addView(ie, 1);
        }
    }

    private ImageView generateElement(Element e) {
        updateTextInfo("" + e, null);

        // Scale elements based on mRatio
        float scaleX = mTranslateX / 2 + mRatio * e.x;
        float scaleY = mTranslateY / 2 + mRatio * e.y;

        float scaleW = mRatio * e.w;
        float scaleH = mRatio * e.h;

        ConstraintLayout.LayoutParams baseParams = (ConstraintLayout.LayoutParams) mBaseImageView.getLayoutParams();

        ImageView view = new ImageView(this);
        ConstraintLayout.LayoutParams params =
                new ConstraintLayout.LayoutParams((int) scaleW, (int) scaleH);
        params.leftToLeft = baseParams.leftToLeft;
        params.topToTop = baseParams.topToTop;
        params.topMargin = (int) scaleY;
        params.leftMargin = (int) scaleX;

        view.setLayoutParams(params);
        // Views are set to CENTER_CROP
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return view;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE) {
                new LoadImage(mSelectedImageView).execute(data.getData());
            }
        }
    }

    private void updateTextInfo(String text, Element e) {
        if (e != null) {
            mTextInfo.setText(mTextInfo.getText() + "\n" + e.prettyName + ": " + text);
        } else {
            mTextInfo.setText(mTextInfo.getText() + "\n" + text);
        }
    }

    @Override
    public void onClick(View view) {
        // Check for type
        mSelectedImageView = (ImageView) view;

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private static class Element {
        int x;
        int y;
        int w;
        int h;

        String prettyName;

        float xRatio = 0;
        float yRatio = 0;

        Element(String prettyName, int x, int y, int w, int h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.prettyName = prettyName;
        }

        @Override
        public String toString() {
            return prettyName + " dimensions: " + w + "x" + h +
                    " | x, y: " + x + ", " + y;
        }
    }

    private class LoadImage extends AsyncTask<Uri, Void, Bitmap> {
        ImageView mImageView;
        int mCurrentIndex;

        LoadImage(ImageView imageView) {
            mImageView = imageView;
            mCurrentIndex = (int) mImageView.getTag();
        }

        @Override
        protected Bitmap doInBackground(Uri... params) {
            return downloadImage(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Element selected = mElements.get(mCurrentIndex);
            if (bitmap != null) {
                if (bitmap.getHeight() < bitmap.getWidth()) {
                    selected.yRatio = selected.h * 1.0f / bitmap.getHeight();
                } else {
                    selected.xRatio = selected.w * 1.0f / bitmap.getWidth();
                }
            }
            mImageView.setImageBitmap(bitmap);
            updateTextInfo("xRatio: " + selected.xRatio + ", yRatio: " + selected.yRatio, selected);
        }

        private Bitmap getBitmapFromUri(Uri uri) throws IOException {
            ParcelFileDescriptor parcelFileDescriptor =
                    getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;
        }

        private Bitmap downloadImage(Uri uri) {
            Bitmap bitmap = null;
            try {
                bitmap = getBitmapFromUri(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }
    }

}
